<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* trick/comments.html.twig */
class __TwigTemplate_8af4ccd667642d5b5b18f5020eb6ae9be8f46006af54f2102e9051b944323a91 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/comments.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/comments.html.twig"));

        // line 1
        echo "   
<div id=\"jq-com\" ";
        // line 2
        if (0 === twig_compare(twig_length_filter($this->env, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 2, $this->source); })())), 5)) {
            echo "class=\"more\"";
        }
        echo ">
    
      ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 4, $this->source); })()), 0, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["comment"]) {
            // line 5
            echo "       <div class=\"alert alert-success\">
          ";
            // line 6
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 6), "picture", [], "any", true, true, false, 6)) {
                // line 7
                echo "            ";
                // line 8
                echo "            <img src=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "user", [], "any", false, false, false, 8), "picture", [], "any", false, false, false, 8), 0, [], "array", false, false, false, 8))), "html", null, true);
                echo "\" class=\"rounded-circle img-fluid\" style=\"max-width: 40px;\">
            ";
            } else {
                // line 10
                echo "            <img src=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/avatar-user.jpg"), "html", null, true);
                echo "\" class=\"rounded-circle img-fluid\" style=\"max-width: 40px;\">
          ";
            }
            // line 12
            echo "
         <strong>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["comment"], "user", [], "any", false, false, false, 13), "nickName", [], "any", false, false, false, 13), "html", null, true);
            echo "</strong><i> à écrit le ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "creationDate", [], "any", false, false, false, 13), "d-m-Y"), "html", null, true);
            echo "</i> : <strong>";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["comment"], "content", [], "any", false, false, false, 13), "html", null, true);
            echo "</strong> 

       </div>

      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['comment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
  </div>
   
  ";
        // line 21
        if ((0 === twig_compare(twig_length_filter($this->env, (isset($context["comments"]) || array_key_exists("comments", $context) ? $context["comments"] : (function () { throw new RuntimeError('Variable "comments" does not exist.', 21, $this->source); })())), 5) && (isset($context["initial_load"]) || array_key_exists("initial_load", $context)))) {
            // line 22
            echo "      <button class=\"btn btn-indigo\" id=\"jq-plus\">Plus</button>
  ";
        }
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "trick/comments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 22,  99 => 21,  94 => 18,  79 => 13,  76 => 12,  70 => 10,  64 => 8,  62 => 7,  60 => 6,  57 => 5,  53 => 4,  46 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("   
<div id=\"jq-com\" {% if (comments|length == 5) %}class=\"more\"{% endif%}>
    
      {% for comment in comments|slice(0,4) %}
       <div class=\"alert alert-success\">
          {% if app.user.picture is defined %}
            {# if user has  no picture stored use default avaatr#}
            <img src=\"{{asset('uploads/images/'~ app.user.picture[0])}}\" class=\"rounded-circle img-fluid\" style=\"max-width: 40px;\">
            {% else %}
            <img src=\"{{asset('img/avatar-user.jpg')}}\" class=\"rounded-circle img-fluid\" style=\"max-width: 40px;\">
          {% endif %}

         <strong>{{comment.user.nickName}}</strong><i> à écrit le {{comment.creationDate|date(\"d-m-Y\")}}</i> : <strong>{{comment.content}}</strong> 

       </div>

      {% endfor %}

  </div>
   
  {% if (comments|length == 5) and (initial_load is defined) %}
      <button class=\"btn btn-indigo\" id=\"jq-plus\">Plus</button>
  {% endif %}", "trick/comments.html.twig", "/home/mouna/perf/mine2/templates/trick/comments.html.twig");
    }
}
