<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* base.html.twig */
class __TwigTemplate_5ca990b8e2b0dbb932585f4fddce29b4df95acf651484a8892e47344c2fe2c02 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'cover' => [$this, 'block_cover'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
\t\t<head>
\t\t  <meta charset=\"utf-8\">
\t\t  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
\t\t  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
\t\t  <title>Welcome to snow tricks</title>
\t\t  <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.0/css/all.css\">
\t\t  <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t  <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/mdb.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t  <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/style.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
      <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/mainstyle.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t\t  ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 14
        echo "
\t\t</head>

    <body>
  <nav class=\"navbar ";
        // line 18
        if (((isset($context["fixed_menu"]) || array_key_exists("fixed_menu", $context)) && 0 === twig_compare((isset($context["fixed_menu"]) || array_key_exists("fixed_menu", $context) ? $context["fixed_menu"] : (function () { throw new RuntimeError('Variable "fixed_menu" does not exist.', 18, $this->source); })()), "enabled"))) {
            echo "fixed-top";
        }
        echo " navbar-expand-lg navbar-dark scrolling-navbar\">
    <div class=\"container\">

      <a class=\"navbar-brand\" href=\"";
        // line 21
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("trick_index");
        echo "\">
        <strong>Snow Tricks</strong>
      </a>

      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\"
        aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
      </button>

      <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">

        <ul class=\"navbar-nav mr-auto\">


      ";
        // line 35
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 36
            echo "
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"";
            // line 38
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("trick_new");
            echo "\">Nouvelle figure</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"";
            // line 41
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil_show");
            echo "\">Mon profil</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"";
            // line 44
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("logout");
            echo "\">Deconnexion</a>
          </li>
      ";
        }
        // line 47
        echo "      ";
        if ( !$this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("ROLE_USER")) {
            // line 48
            echo "          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"";
            // line 49
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("user_registration");
            echo "\">Inscription</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"";
            // line 52
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("login");
            echo "\">Connexion</a>
          </li>
      ";
        }
        // line 55
        echo "        </ul>


      
      </div>

    </div>
  </nav>

";
        // line 64
        $this->displayBlock('cover', $context, $blocks);
        // line 93
        echo "
  
   ";
        // line 95
        $this->displayBlock('body', $context, $blocks);
        // line 96
        echo "

  <footer class=\"page-footer text-center font-small mt-4 wow fadeIn fixed-bottom\">

    <div class=\"footer-copyright py-3\">
      © 2019 Copyright:
      <a href=\"https://www.linkedin.com/in/moez-thabti-3949b915a/\" target=\"_blank\"> Moez Thabti : Projet d'étude </a>
    </div>

  </footer>


        <!-- JQuery -->
\t\t\t\t<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
\t\t\t\t<!-- Bootstrap tooltips -->
\t\t\t\t<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js\"></script>
\t\t\t\t<!-- Bootstrap core JavaScript -->
\t\t\t\t<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js\"></script>
\t\t\t\t<!-- MDB core JavaScript -->
\t\t\t\t<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.6.1/js/mdb.min.js\"></script>
\t\t\t\t<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js\"></script>
\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t
\t\t\t\t\t";
        // line 119
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 119, $this->source); })()), "session", [], "any", false, false, false, 119), "flashbag", [], "any", false, false, false, 119), "all", [], "method", false, false, false, 119));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 120
            echo "\t\t   \t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 121
                echo "\t\t   \t\t\t\t\t\tnew Noty({
\t\t   \t\t\t\t\t\t animation: {
                        open: 'animated bounceInRight', // Animate.css class names
                        close: 'animated bounceOutRight' // Animate.css class names
                     },
    \t\t\t\t\t\t\ttext: '<div class=\"text-center alert alert-";
                // line 126
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                echo "</div>',
\t\t\t\t\t\t\t\t\t}).show();
\t\t\t\t\t   ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 129
            echo " \t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "        </script>
        <script type=\"text/javascript\" src=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/mainJs.js"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/new_collection_widget.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 133
        $this->displayBlock('javascripts', $context, $blocks);
        // line 134
        echo "    </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 64
    public function block_cover($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "cover"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "cover"));

        // line 65
        echo "  <div class=\"view full-page-intro\" style=\"background-image: url(";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/tricks-HPage.jpg"), "html", null, true);
        echo ") ; background-repeat: no-repeat; background-size: cover; background-position: center; \">

    <div class=\"mask rgba-black-light d-flex justify-content-center align-items-center\">

      <div class=\"container\">

        <div class=\"row wow fadeIn\">

          <div class=\"col-lg-12 col-md-7 mb-4 white-text text-center text-md-left\">

            <h1 class=\"display-4 font-weight-bold\">Le site des artistes du ski</h1>

            <hr class=\"hr-light\">

            <p class=\"mb-4 d-none d-md-block\">
              <strong>Le ski est plus qu'un loisir pour vous ? <br/>Rejoignez notre communauté dediée au ski acrobatique</strong>
            </p>
          </div>

        </div>

      </div>

    </div>
     
  </div>
 
  ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 95
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 133
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  335 => 133,  317 => 95,  278 => 65,  268 => 64,  250 => 13,  238 => 134,  236 => 133,  232 => 132,  228 => 131,  225 => 130,  219 => 129,  208 => 126,  201 => 121,  196 => 120,  192 => 119,  167 => 96,  165 => 95,  161 => 93,  159 => 64,  148 => 55,  142 => 52,  136 => 49,  133 => 48,  130 => 47,  124 => 44,  118 => 41,  112 => 38,  108 => 36,  106 => 35,  89 => 21,  81 => 18,  75 => 14,  73 => 13,  69 => 12,  65 => 11,  61 => 10,  57 => 9,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"fr\">
\t\t<head>
\t\t  <meta charset=\"utf-8\">
\t\t  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
\t\t  <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\">
\t\t  <title>Welcome to snow tricks</title>
\t\t  <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.0/css/all.css\">
\t\t  <link href=\"{{asset('css/bootstrap.min.css')}}\" rel=\"stylesheet\">
\t\t  <link href=\"{{asset('css/mdb.min.css')}}\" rel=\"stylesheet\">
\t\t  <link href=\"{{asset('css/style.min.css')}}\" rel=\"stylesheet\">
      <link href=\"{{asset('css/mainstyle.css')}}\" rel=\"stylesheet\">
\t\t  {% block stylesheets %}{% endblock %}

\t\t</head>

    <body>
  <nav class=\"navbar {% if fixed_menu is defined and fixed_menu == 'enabled' %}fixed-top{%endif%} navbar-expand-lg navbar-dark scrolling-navbar\">
    <div class=\"container\">

      <a class=\"navbar-brand\" href=\"{{path('trick_index')}}\">
        <strong>Snow Tricks</strong>
      </a>

      <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\"
        aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        <span class=\"navbar-toggler-icon\"></span>
      </button>

      <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">

        <ul class=\"navbar-nav mr-auto\">


      {% if is_granted('ROLE_USER') %}

          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"{{path('trick_new')}}\">Nouvelle figure</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"{{path('profil_show')}}\">Mon profil</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"{{path('logout')}}\">Deconnexion</a>
          </li>
      {% endif %}
      {% if not is_granted('ROLE_USER') %}
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"{{path('user_registration')}}\">Inscription</a>
          </li>
          <li class=\"nav-item\">
            <a class=\"nav-link\" href=\"{{path('login')}}\">Connexion</a>
          </li>
      {% endif %}
        </ul>


      
      </div>

    </div>
  </nav>

{% block cover%}
  <div class=\"view full-page-intro\" style=\"background-image: url({{ asset('img/tricks-HPage.jpg') }}) ; background-repeat: no-repeat; background-size: cover; background-position: center; \">

    <div class=\"mask rgba-black-light d-flex justify-content-center align-items-center\">

      <div class=\"container\">

        <div class=\"row wow fadeIn\">

          <div class=\"col-lg-12 col-md-7 mb-4 white-text text-center text-md-left\">

            <h1 class=\"display-4 font-weight-bold\">Le site des artistes du ski</h1>

            <hr class=\"hr-light\">

            <p class=\"mb-4 d-none d-md-block\">
              <strong>Le ski est plus qu'un loisir pour vous ? <br/>Rejoignez notre communauté dediée au ski acrobatique</strong>
            </p>
          </div>

        </div>

      </div>

    </div>
     
  </div>
 
  {% endblock %}

  
   {% block body %}{% endblock %}


  <footer class=\"page-footer text-center font-small mt-4 wow fadeIn fixed-bottom\">

    <div class=\"footer-copyright py-3\">
      © 2019 Copyright:
      <a href=\"https://www.linkedin.com/in/moez-thabti-3949b915a/\" target=\"_blank\"> Moez Thabti : Projet d'étude </a>
    </div>

  </footer>


        <!-- JQuery -->
\t\t\t\t<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
\t\t\t\t<!-- Bootstrap tooltips -->
\t\t\t\t<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js\"></script>
\t\t\t\t<!-- Bootstrap core JavaScript -->
\t\t\t\t<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js\"></script>
\t\t\t\t<!-- MDB core JavaScript -->
\t\t\t\t<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.6.1/js/mdb.min.js\"></script>
\t\t\t\t<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js\"></script>
\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t
\t\t\t\t\t{% for type, messages in app.session.flashbag.all() %}
\t\t   \t\t\t\t{% for message in messages %}
\t\t   \t\t\t\t\t\tnew Noty({
\t\t   \t\t\t\t\t\t animation: {
                        open: 'animated bounceInRight', // Animate.css class names
                        close: 'animated bounceOutRight' // Animate.css class names
                     },
    \t\t\t\t\t\t\ttext: '<div class=\"text-center alert alert-{{type}}\">{{message}}</div>',
\t\t\t\t\t\t\t\t\t}).show();
\t\t\t\t\t   {% endfor %}
 \t\t\t\t\t{% endfor %}
        </script>
        <script type=\"text/javascript\" src=\"{{asset('js/mainJs.js')}}\"></script>
        <script type=\"text/javascript\" src=\"{{asset('js/new_collection_widget.js')}}\"></script>
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/mouna/perf/mine2/templates/base.html.twig");
    }
}
