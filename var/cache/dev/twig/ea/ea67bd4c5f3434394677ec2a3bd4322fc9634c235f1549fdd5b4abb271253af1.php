<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* user/profile.html.twig */
class __TwigTemplate_8c63a18f8acc9dbe06367f582206a495f26d3bc6202d0edda52e0e050f070659 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'cover' => [$this, 'block_cover'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/profile.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "user/profile.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "user/profile.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_cover($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "cover"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "cover"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "

<section class=\"text-center my-5 p-1\">

  <!-- Section heading -->
  <h1 class=\"h1-responsive font-weight-bold my-5\">Mon profil</h1>

  <!-- Grid row -->
  <div class=\"row d-flex justify-content-center\">

    <!--Grid column-->
    <div class=\"col-lg-6 col-lg-offset-3 col-md-12 mb-lg-0 mb-4 d-flex justify-content-center\">
      <!--Card-->
      <div class=\"card testimonial-card\">
        <!--Background color-->
        <div class=\"card-up info-color\"></div>
        <!--Avatar-->
        <div class=\"avatar mx-auto white pt-2\">

          ";
        // line 24
        if (1 === twig_compare(twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 24, $this->source); })()), "user", [], "any", false, false, false, 24), "picture", [], "any", false, false, false, 24)), 0)) {
            // line 25
            echo "            ";
            // line 26
            echo "          <img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/images/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 26, $this->source); })()), "user", [], "any", false, false, false, 26), "picture", [], "any", false, false, false, 26), 0, [], "array", false, false, false, 26))), "html", null, true);
            echo "\" class=\"rounded-circle img-fluid\" style=\"max-width: 180px;\">
          ";
        } else {
            // line 28
            echo "          <img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/avatar-user.jpg"), "html", null, true);
            echo "\" class=\"rounded-circle img-fluid\" style=\"max-width: 180px;\">
          ";
        }
        // line 30
        echo "
        </div>
        <div class=\"card-body\">
          <!--Name-->
          <span class=\"font-weight-bold mb-4\">Nom d'utilisateur : </span><span>";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 34, $this->source); })()), "user", [], "any", false, false, false, 34), "nickname", [], "any", false, false, false, 34), "html", null, true);
        echo "</span><br/>
          <span class=\"font-weight-bold mb-4\">Email : </span><span>";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 35, $this->source); })()), "user", [], "any", false, false, false, 35), "email", [], "any", false, false, false, 35), "html", null, true);
        echo "</span>
          <hr>
          <!--Quotation-->
          <p class=\"dark-grey-text mt-4\">
            <a type=\"button\" class=\"btn btn-primary\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profil_edit", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 39, $this->source); })()), "user", [], "any", false, false, false, 39), "id", [], "any", false, false, false, 39)]), "html", null, true);
        echo "\">Modifier vos informations</a>
          </p>
        </div>
      </div>
      <!--Card-->
    </div>
    <!--Grid column-->

  </div>
  <!-- Grid row -->

</section>
<!-- Section: Testimonials v.1 -->

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "user/profile.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 39,  134 => 35,  130 => 34,  124 => 30,  118 => 28,  112 => 26,  110 => 25,  108 => 24,  87 => 5,  77 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block cover %}{% endblock %}
{% block body %}


<section class=\"text-center my-5 p-1\">

  <!-- Section heading -->
  <h1 class=\"h1-responsive font-weight-bold my-5\">Mon profil</h1>

  <!-- Grid row -->
  <div class=\"row d-flex justify-content-center\">

    <!--Grid column-->
    <div class=\"col-lg-6 col-lg-offset-3 col-md-12 mb-lg-0 mb-4 d-flex justify-content-center\">
      <!--Card-->
      <div class=\"card testimonial-card\">
        <!--Background color-->
        <div class=\"card-up info-color\"></div>
        <!--Avatar-->
        <div class=\"avatar mx-auto white pt-2\">

          {% if app.user.picture |length > 0 %}
            {# if user has  no picture stored use default avaatr#}
          <img src=\"{{asset('uploads/images/'~ app.user.picture[0])}}\" class=\"rounded-circle img-fluid\" style=\"max-width: 180px;\">
          {% else %}
          <img src=\"{{asset('img/avatar-user.jpg')}}\" class=\"rounded-circle img-fluid\" style=\"max-width: 180px;\">
          {% endif %}

        </div>
        <div class=\"card-body\">
          <!--Name-->
          <span class=\"font-weight-bold mb-4\">Nom d'utilisateur : </span><span>{{app.user.nickname}}</span><br/>
          <span class=\"font-weight-bold mb-4\">Email : </span><span>{{app.user.email}}</span>
          <hr>
          <!--Quotation-->
          <p class=\"dark-grey-text mt-4\">
            <a type=\"button\" class=\"btn btn-primary\" href=\"{{path('profil_edit', {'id' : app.user.id})}}\">Modifier vos informations</a>
          </p>
        </div>
      </div>
      <!--Card-->
    </div>
    <!--Grid column-->

  </div>
  <!-- Grid row -->

</section>
<!-- Section: Testimonials v.1 -->

{% endblock %}
", "user/profile.html.twig", "/home/mouna/perf/mine2/templates/user/profile.html.twig");
    }
}
