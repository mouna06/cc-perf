<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* trick/index.html.twig */
class __TwigTemplate_b9bab9bbcc8dfbbc994fcb89e76b4d253a57875bb5782fef214040f4941dd4b0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "trick/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "trick/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Trick index";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
<section>

  <div class=\"container\">
    <div>
      <div class=\"d-flex row justify-content-start mx-auto\" id=\"jq-card\">    
      </div>

      <div class=\"mx-auto text-center\">
        <img src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/loading_spinner.gif"), "html", null, true);
        echo "\" id=\"spin-img\">
      </div>

    </div>
     
    <button class=\"btn btn-default\" id =\"js-scroll-down\"><i class=\"fas fa-arrow-down ml-1\"></i></button>
  </div>
</div>

  
</section>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 29
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 30
        echo "<script type=\"text/javascript\">

  \$(document).ready(function(){
    
    \$('#spin-img').hide()
    var tricks_count;

    // initial request to get the tricks'number from db
    \$.post( \"";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("trick_ajax");
        echo "\")
      .done(function(count){  tricks_count = parseInt(count);})

    if (tricks_count == 0) {\$('#js-scroll-down').hide();}

    \$('#js-scroll-down').on('click', function(e){

      \$('#spin-img').show()
      var appended_tricks = \$('#jq-card > div >.card').length;
     
      \$.post( \"";
        // line 48
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("trick_ajax");
        echo "\", {first: appended_tricks})
        .done(function(tricks){
          \$('#spin-img').hide()
          \$('#jq-card').append(tricks);
          var total_tricks = \$('#jq-card > div >.card').length;
          // if there is no more tricks in the db hide button
          if (total_tricks == tricks_count) {
            \$('#js-scroll-down').hide();
          }   
        })
      // scroll page down whenever the loading button is clicked
      var n = \$(document).height();
      \$('html, body').animate({ scrollTop:  \$('body').offset().top + n }, 'slow');
    });
  });
  </script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "trick/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 48,  143 => 38,  133 => 30,  123 => 29,  100 => 15,  89 => 6,  79 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Trick index{% endblock %}

{% block body %}

<section>

  <div class=\"container\">
    <div>
      <div class=\"d-flex row justify-content-start mx-auto\" id=\"jq-card\">    
      </div>

      <div class=\"mx-auto text-center\">
        <img src=\"{{ asset('img/loading_spinner.gif')}}\" id=\"spin-img\">
      </div>

    </div>
     
    <button class=\"btn btn-default\" id =\"js-scroll-down\"><i class=\"fas fa-arrow-down ml-1\"></i></button>
  </div>
</div>

  
</section>

{% endblock %}

{% block javascripts %}
<script type=\"text/javascript\">

  \$(document).ready(function(){
    
    \$('#spin-img').hide()
    var tricks_count;

    // initial request to get the tricks'number from db
    \$.post( \"{{path('trick_ajax')}}\")
      .done(function(count){  tricks_count = parseInt(count);})

    if (tricks_count == 0) {\$('#js-scroll-down').hide();}

    \$('#js-scroll-down').on('click', function(e){

      \$('#spin-img').show()
      var appended_tricks = \$('#jq-card > div >.card').length;
     
      \$.post( \"{{path('trick_ajax')}}\", {first: appended_tricks})
        .done(function(tricks){
          \$('#spin-img').hide()
          \$('#jq-card').append(tricks);
          var total_tricks = \$('#jq-card > div >.card').length;
          // if there is no more tricks in the db hide button
          if (total_tricks == tricks_count) {
            \$('#js-scroll-down').hide();
          }   
        })
      // scroll page down whenever the loading button is clicked
      var n = \$(document).height();
      \$('html, body').animate({ scrollTop:  \$('body').offset().top + n }, 'slow');
    });
  });
  </script>
{% endblock %}", "trick/index.html.twig", "/home/mouna/perf/mine2/templates/trick/index.html.twig");
    }
}
