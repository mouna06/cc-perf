<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerXSVzL4U\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerXSVzL4U/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerXSVzL4U.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerXSVzL4U\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerXSVzL4U\App_KernelDevDebugContainer([
    'container.build_hash' => 'XSVzL4U',
    'container.build_id' => '61ee38b4',
    'container.build_time' => 1587923542,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerXSVzL4U');
