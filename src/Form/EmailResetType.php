<?php

namespace App\Form;

use Symfony\Component\Form\{AbstractType,FormBuilderInterface};

class EmailResetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email');
    }
}
